function isString(value) {
    return typeof value === "string";
}

function isNumber(value) {
    return Number.isInteger(value);
}

const TypeError = (field, expectedType, type) => {
    this.message = `${Object.keys(field)[0]} must be ${expectedType}. Found ${type}`;
}

const TIMEOUT = 100;


function checkType(variable, expectedType){
    return new Promise((resolve,) => {
        if (typeof variable === expectedType){
            resolve();
        } else {
            throw new TypeError(variable, expectedType, typeof variable);
        }
    });
}


class Dealer{
    #title;
    #vehicles;

    constructor(title) {
        this.title = title;
        this.#vehicles = [];
    }

    set title(newTitle) {
        checkType(newTitle, 'string').then(() => this.#title = newTitle);
    }

    get title() {
        return this.#title;
    }

    get vehicles(){
        return this.#vehicles.map(x => x.toString());
    }

    addVehicle(vehicle) {
        return new Promise((resolve, reject) => {
            if(vehicle instanceof Vehicle){
                setTimeout(() => {
                    if(this.#vehicles.filter((item) => item.vin === vehicle.vin).length !== 0) {
                        reject('There is such vehicle with this VIN. Please try again.');
                        return;
                    }
                    this.#vehicles.push(vehicle);
                    resolve(true);
                }, TIMEOUT);
            } else {
                throw new TypeError(vehicle, 'Vehicle', typeof vehicle);
            }
        })
    }

    sellVehicle(vin){
        return new Promise((resolve, reject) => {
            checkType(vin, 'number').then(() => {
                if(this.#vehicles.filter((item) => item.vin === vin).length !== 0) {
                    reject('There is such vehicle with this VIN. Please try again.');
                    return;
                }
                setTimeout(() => {
                    this.#vehicles = this.#vehicles.filter(x => x.vin !== vin);
                    console.log(`Truck with VIN=${vin} is sold successfully`)
                    resolve();
                }, TIMEOUT);
            })
        })
    }

    findTruck(carryWeight, color){
        return new Promise((resolve, reject) => {
            checkType(carryWeight, 'number').then(() => {
                checkType(color, 'string').then(() => {
                    setTimeout(() => {
                        const truck = this.#vehicles.find((x) => x instanceof Truck && x.color === color && x.carryWeight === carryWeight);
                        truck
                            ? resolve(truck)
                            : reject("Cannot find this truck!");
                    }, TIMEOUT);
                });
            });
        });
    }

    paintBus(vin, color){
        return new Promise((resolve, reject) => {
            checkType(vin, 'number').then(() => {
                checkType(color, 'string').then(() => {
                    setTimeout(() => {
                        const bus = this.#vehicles.find((item) =>  item instanceof Bus && item.vin === vin);
                        if(bus) {
                            bus.color = color;
                            resolve();
                        } else {
                            reject('Cannot find this bus!');
                        }
                    }, TIMEOUT);
                });
            });
        })
    }

    countVehiclesWithColor(color) {
        return new Promise((resolve) => {
            checkType(color, 'string').then(() => {
                setTimeout(() => {
                    resolve(this.#vehicles.filter(v => v.color === color).length);
                }, TIMEOUT);
            });
        })
    }
}




class Vehicle {
    #vin;
    #color;

    constructor(vin, color){
        this.vin = vin;
        this.color = color;
    }

    set vin(vin){
        checkType(vin, 'number').then(() => this.#vin = vin);
    }
    get vin() {
        return this.#vin;
    }

    set color(color){
        checkType(color, 'string').then(() => this.#color = color);
    }

    get color() {
        return this.#color;
    }
}


class Truck extends Vehicle{
    #carryWeight;

    constructor(vin, color, carryWeight) {
        super(vin, color);
        this.carryWeight = carryWeight;
    }

    set carryWeight(carryWeight) {
        checkType(carryWeight, 'number').then(() => this.#carryWeight = carryWeight);
    }

    get carryWeight(){
        return this.#carryWeight;
    }

    toString(){
        return `Truck[${this.vin}] color: ${this.color}, carryWeight:${this.carryWeight}`;
    }
}


class Bus extends Vehicle{
    #maxPassengers;

    constructor(vin, color, maxPassengers) {
        super(vin, color);
        this.maxPassengers = maxPassengers;
    }

    set maxPassengers(maxPassengers) {
        checkType(maxPassengers, 'number').then(() => this.#maxPassengers = maxPassengers);
    }

    get maxPassengers(){
        return this.#maxPassengers;
    }

    toString(){
        return `Bus[${this.vin}] Color: ${this.color}, maxPassengers: ${this.maxPassengers}`;
    }
}


const DATABASE = {
    dealer: {
        title: 'Trucks & Buses Vilnius LTD'
    },
    trucks: [
        {
            vin: 1112,
            color: 'Red',
            carryWeight: 10
        },
        {
            vin: 2332,
            color: 'Yellow',
            carryWeight: 20
        },
        {
            vin: 5234,
            color: 'Green',
            carryWeight: 70
        }
    ],
    buses: [
        {
            vin: 1112,
            color: 'Green',
            maxPassengers: 50
        },
        {
            vin: 6543,
            color: 'Yellow',
            maxPassengers: 25
        }
    ]
}

// Set objects from DB
const dealer = new Dealer(DATABASE.dealer.title);

const trucks = DATABASE.trucks.map(truck => new Truck(truck.vin, truck.color, truck.carryWeight));
const buses = DATABASE.buses.map(bus => new Bus(bus.vin, bus.color, bus.maxPassengers));
trucks.forEach(x => dealer.addVehicle(x).catch(console.error));
buses.forEach(x => dealer.addVehicle(x).catch(console.error));

console.log(dealer.vehicles);
dealer.findTruck(10, 'Red').then(truck => {
    console.log(dealer.vehicles);
    console.log(truck.toString());
    dealer.sellVehicle(truck.vin).then(() => {
        console.log(dealer.vehicles);
    }).catch(console.error);
}).catch(console.error);

setTimeout(() => {
    dealer.countVehiclesWithColor('Green').then(count => {
        console.log(`Count of vehicles with color 'Green' = ${count}`);
        dealer.paintBus(1234, 'Red').then(() => {
            dealer.countVehiclesWithColor('Green').then(count2 => {
                console.log(`Count of vehicles with color 'Green' = ${count2}`);
                dealer.addVehicle(new Truck(666, 'Green', 777)).then(() => {
                    dealer.countVehiclesWithColor('Green').then(count3 => {
                        console.log(`Count of vehicles with color 'Green' = ${count3}`);
                    }).catch(console.error);
                }).catch(console.error);
            }).catch(console.error);
        }).catch(console.error);
    }).catch(console.error);
}, TIMEOUT*4)
